#!/usr/bin/env python

from distutils.core import setup
from setuptools import find_packages

setup(name='pyidcontroller',
      version='1.0',
      description='Simple PID controller',
      author='csblo',
      author_email='',
      url='http://www.csblo.fr',
      packages=find_packages(exclude=('screenshots',)))
